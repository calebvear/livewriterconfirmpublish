﻿using System;
using System.Collections.Generic;
using System.Text;
using WindowsLive.Writer.Api;
using System.Windows.Forms;

namespace ConfirmPlublishPlugin
{
    [WriterPlugin("4210579C-EAC4-4ccb-83FD-A90C58B4020C", "Confirms publish action")]
    public class ConfirmPublish : PublishNotificationHook
    {
        public override bool OnPrePublish(System.Windows.Forms.IWin32Window dialogOwner, IProperties properties, IPublishingContext publishingContext, bool publish)
        {
            if (MessageBox.Show(dialogOwner, "So Boni... do you actually want to publish this?", "Confirm Publish", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                return base.OnPrePublish(dialogOwner, properties, publishingContext, publish);
            else
                return false;
        }
    }
}
