Live Writer Confirm Publish Plugin
==================================

I really simple plugin which shows a dialog before publishing a blog post asking you to confirm you want to publish.  I created this plugin for my wife as she was often accidentally publishing posts instead of saving them.